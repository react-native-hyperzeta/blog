import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput ,Button} from 'react-native'

const BlogPostForm = ({onSubmit, initialValues}) => {

    const [title, setTitle] = useState(initialValues.title)
    const [content, setContent] = useState(initialValues.content)

    return (
        <View>
        <Text style={styles.textStyle}>Title:</Text>
        <TextInput style={styles.textInputStyle} value={title} onChangeText={text=>setTitle(text)}/>
        <Text style={styles.textStyle}>Content:</Text>
        <TextInput style={styles.textInputStyle} value={content} onChangeText={text=>setContent(text)}/>
        <Button style={{alignSelf: 'center'}} title='Add' onPress={()=>{onSubmit(title, content)}}/>
    </View>
    )
}

export default BlogPostForm

BlogPostForm.defaultProps = {
    initialValues: {
        title: '',
        content: ''
    },
}

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 15,
        margin: 5,
        fontWeight: 'bold'
    },
    inputStyle: {
        fontSize: 15,
        borderWidth: 1,
        borderColor: 'grey',
        padding: 5,
        margin: 5,
        marginBottom: 30
    }
})
