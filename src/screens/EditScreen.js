import React, {useContext} from 'react'
import { StyleSheet, Text, View, TextInput, Button } from 'react-native'
import BlogContext from '../contexts/BlogContext'
import BlogPostForm from '../components/BlogPostForm'

const EditScreen = ({navigation}) => {
    const id = navigation.getParam('id')
    const  {data, editBlogPost} = useContext(BlogContext)
    const blogPost = data.find((blogPost) => blogPost.id === id)

    return (
        <BlogPostForm
            initialValues = {{title: blogPost.title, content: blogPost.content}}
            onSubmit = {(title, content)=>{
                editBlogPost(id, title, content, ()=>{navigation.pop()})
        }} />
    )
}

export default EditScreen

const styles = StyleSheet.create({})
