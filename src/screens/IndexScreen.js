import React, {useContext, useEffect} from 'react'
import { StyleSheet, Text, View, FlatList, Button, TouchableOpacity } from 'react-native'
import BlogContext from '../contexts/BlogContext'
import { Feather } from '@expo/vector-icons'

const IndexScreen = ({navigation}) => {
    const {data, deleteBlogPost, getBlogPosts} = useContext(BlogContext)

    useEffect(() =>{
        getBlogPosts();
        const listener = navigation.addListener('didFocus',()=>{
            getBlogPosts();
        })
        return ()=>{
            listener.remove();
        }
    },[]);

    return (
        <View>
            <FlatList
            data = {data}
            keyExtractor = {(blogpost) => blogpost.title} // Won't work: onPress={navigation.navigate('Show',{id: item.id})}
            renderItem = {({item}) =>{
                return(
                <TouchableOpacity onPress={()=>navigation.navigate('Show',{id: item.id})}>
                    <View style={styles.viewStyle}>
                        <Text style= {styles.titleStyle}>{item.title}</Text>
                        <TouchableOpacity onPress={()=>{deleteBlogPost(item.id)}}>
                            <Feather name="trash" size={25} color="red" />
                        </TouchableOpacity>
                    </View>
                </TouchableOpacity>
                )
            }}
            />
        </View>
    )
}



IndexScreen.navigationOptions = ({navigation})=>{
    return{
        headerRight: () => (
            <TouchableOpacity onPress={()=>navigation.navigate('Create')}>
                <Feather style={{marginRight: 15}} name="plus" size={30} />
            </TouchableOpacity>
        )
    }
}

export default IndexScreen;

const styles = StyleSheet.create({
    viewStyle:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 15,
        paddingBottom: 15,
        borderColor: 'gray'
    },
    titleStyle:{
        fontSize: 15,
        fontWeight: 'bold'
    }
})
