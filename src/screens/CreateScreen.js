import React, {useContext, useState} from 'react'
import { StyleSheet, Text, View, TextInput ,Button} from 'react-native'
import BlogContext from '../contexts/BlogContext'
import BlogPostForm from '../components/BlogPostForm'

const CreateScreen = ({navigation}) => {
    const  {addBlogPost} = useContext(BlogContext)

    return (
       <BlogPostForm onSubmit={(title, content) =>{
           addBlogPost(title, content, ()=>{navigation.navigate('Index')})
       }}/>
    )
}

export default CreateScreen

const styles = StyleSheet.create({})
