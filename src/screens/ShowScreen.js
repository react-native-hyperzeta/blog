import React, {useContext} from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import BlogContext from '../contexts/BlogContext'
import {Feather} from '@expo/vector-icons'

const ShowScreen = ({navigation}) => {
    const id = navigation.getParam('id');
    const {data} = useContext(BlogContext)
    const blogPost = data.find((blogPost)=>blogPost.id === id)

    return (
        <View>
            <Text>{blogPost.title}</Text>
            <Text>{blogPost.content}</Text>
        </View>
    )
}

ShowScreen.navigationOptions = ({navigation})=>{
    return{
        headerRight: ()=>(
            <TouchableOpacity onPress={()=>navigation.navigate('Edit', {id: navigation.getParam('id')})}>
                <Feather style={{marginRight: 15}} name="edit" size={30} />
            </TouchableOpacity>
        )
    }
}

export default ShowScreen

const styles = StyleSheet.create({})
