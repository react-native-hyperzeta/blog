import React, {useReducer} from 'react'
import jsonServer from '../api/jsonServer'

const BlogContext = React.createContext()

const reducer = (state, action)=>{
        switch(action.type){
            case "get_blogposts":
                return action.payload;
            case "add_blogpost":
                return [...state, {id: Math.floor(Math.random()*99999), title: action.payload.title, content: action.payload.content}];
            case "edit_blogpost":
                return state.map((blogPost)=>{
                    return (blogPost.id === action.payload.id) ? action.payload : blogPost
                });
            case "delete_blogpost":
                return state.filter((blogPost)=>blogPost.id !== action.payload);  // Won't work (state.filter((blogPost)=>{blogPost.id !== action.payload}))
            default:
                return state
        }
}

export const BlogProvider = ({children}) => {
    const [BlogPosts, dispatch] = useReducer(reducer, [])

    const getBlogPosts = async()=>{
        const response = await jsonServer.get('/blogposts')
        dispatch({type: "get_blogposts", payload: response.data})
    }

    const addBlogPost = async (title, content, callback) => {
        await jsonServer.post('/blogposts',{title,content})
        if(callback){
            callback();
        }
        // dispatch({type: "add_blogpost", payload: {title, content}});
    }
    const deleteBlogPost = async (id)=>{
        await jsonServer.delete(`/blogposts/${id}`)
        dispatch({type: "delete_blogpost", payload: id});
    }
    const editBlogPost = async (id, title, content, callback)=> {
        await jsonServer.put(`/blogposts/${id}`, {title, content})
        dispatch({type: "edit_blogpost", payload: {id, title, content}});
        if(callback){
            callback();
        }
    }
    return(
        <BlogContext.Provider value={{data: BlogPosts, addBlogPost, deleteBlogPost, editBlogPost, getBlogPosts}}>
            {children}
        </BlogContext.Provider>
    )
}

export default BlogContext;